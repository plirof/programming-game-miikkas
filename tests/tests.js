/*
 * Tests for the platformer, utilizing QUnit.
 */
module("overall");
test("namespace test", function() {
    ok(platformer, "Namespace platformer found");
});

var plf = platformer;

/* 
 * Test the constants module.
 */
module("constants.js");
test("element constant existance test", function() {
    ok(plf.PASS, "PASS found");
    ok(plf.FOR_JUMP, "FOR_JUMP found");
    ok(plf.FOR_STMT, "FOR_STMT found");
    ok(plf.PUSH, "PUSH found");
    ok(plf.POP, "POP found");
    ok(plf.DELETE, "DELETE found");
    ok(plf.ASSIGN, "ASSIGN found");
    ok(plf.PUSH_RANDOM, "PUSH_RANDOM found");
    ok(plf.FUNC_CALL, "FUNC_CALL found");
    ok(plf.RETURN_STMT, "RETURN_STMT found");
    ok(plf.CONDITIONAL, "CONDITIONAL found");
    ok(plf.SYS_EXIT, "SYS_EXIT found");
});
test("color constant test", function() {
    ok(plf.RED, "RED found");
    ok(plf.BLUE, "BLUE found");
    ok(plf.GREEN, "GREEN found");
    ok(plf.YELLOW, "YELLOW found");
    deepEqual(plf.COLORS, [plf.RED, plf.BLUE, plf.GREEN, plf.YELLOW], "COLORS contains the listed colors");
});
test("element component constant existance test", function() {
    ok(plf.ELEMENT, "ELEMENT found");
    ok(plf.ARGS, "ARGS found");
    ok(plf.ITERATOR, "ITERATOR found");
    ok(plf.RETURN_VAL, "RETURN_VAL found");
});
test("level frames and animations test", function() {
    ok(plf.LEVEL_FRAMES, "LEVEL_FRAMES found");
    ok(plf.LEVEL_ANIMATIONS, "LEVEL_ANIMATIONS found");
    equal(plf.LEVEL_FRAMES.length, Object.keys(plf.LEVEL_ANIMATIONS).length, "FRAMES and ANIMATIONS equal lenght");
});
test("menu frames and animations test", function() {
    ok(plf.MENU_FRAMES, "MENU_FRAMES found");
    ok(plf.MENU_ANIMATIONS, "MENU_ANIMATIONS found");
    equal(plf.MENU_FRAMES.length, Object.keys(plf.MENU_ANIMATIONS).length, "FRAMES and ANIMATIONS equal lenght");
});

/*
 * Test the utils module.
 */
module("utils.js");
test("prototyped array test", function() {
    var test_array = ["a", "b", "c", "d"];
    ok(test_array.remove, "remove() method found");
    test_array.remove("a");
    deepEqual(test_array, ["b", "c", "d"], "removing first item successful");
    test_array.remove("c");
    deepEqual(test_array, ["b", "d"], "removing middle item successful");
    test_array.remove("d");
    deepEqual(test_array, ["b"], "removing last item successful");

    test_array = ["a", "b", "c", "d"];
    ok(test_array.clone, "clone() method found");
    deepEqual(test_array.clone(), ["a", "b", "c", "d"], "array successfully cloned");

    test_array = ["a", "b", "c", "d"];
    ok(test_array.omit, "omit() method found");
    deepEqual(test_array.omit("b"), ["a", "c", "d"], "array successfully cloned with an item omitted");

    test_array = ["a", "b", "c", "d"];
    ok(test_array.extend, "extend() method found");
    test_array.extend(["x", "y"]);
    deepEqual(test_array, ["a", "b", "c", "d", "x", "y"], "array successfully extended");

    test_array = ["a", "b", "c", "d"];
    ok(test_array.contains, "contains() method found");
    equal(test_array.contains("a"), true, "item in array found correctly");
    equal(test_array.contains("x"), false, "item not in array correctly not found");

    test_array = ["a", "b", "c", "d"];
    ok(test_array.get_last, "get_last() method found");
    equal(test_array.get_last(), "d", "last item successfully got");
});
test("prototyped string test", function() {
    var test_string = "";
    ok(test_string.repeat, "repeat() method found");
    equal("test".repeat(3), "testtesttest", "call with parameter 3 successful");
    equal("test".repeat(0), "", "call with parameter 0 successful");
    equal("test".repeat(NaN), "", "call with parameter NaN successful");
});
test("default_to function test", function() {
    ok(plf.default_to, "default_to() function found");
    equal(plf.default_to(undefined, "default value"), "default value", "undefined parameter defaulted");
    equal(plf.default_to("undefined", "default value"), "undefined", "\"undefined\" string not defaulted");
});

/*
 * Test the levelnode module.
 */
(function() {
    var tn1;
    var tn2;
    var tn3;
    var retval;

    module("levelnode.js", {
        setup: function() {
            tn1 = new plf.LevelNode();
            tn2 = new plf.LevelNode();
            tn3 = new plf.LevelNode();
            retval = undefined;
        }
    });

    test("node creation test", function() {
        ok(tn1, "test node 1 successfully created");
        ok(tn2, "test node 2 successfully created");
        ok(tn3, "test node 3 successfully created");
    });
    
    test("node find by branch test", function() {
        retval = plf.LevelNode.find_node_by_branch([], 1);
        equal(retval, null, "find_node_by_branch() returned null on empty list");
        retval = plf.LevelNode.find_node_by_branch([tn3], 1);
        equal(retval, tn3, "find_node_by_branch() returned test node with a list containing only test node");

        tn1.branch = 1;
        tn2.branch = 2;
        retval = plf.LevelNode.find_node_by_branch([tn1, tn2], 1);
        equal(retval, tn2, "find_node_by_branch() successfully returned test node with higher branch value");
        retval = plf.LevelNode.find_node_by_branch([tn1, tn2], -1);
        equal(retval, tn1, "find_node_by_branch() successfully returned test node with lower branch value");
    });

    test("highest node find test", function() {
        tn1.branch = 1;
        tn2.branch = 2;
        retval = plf.LevelNode.find_highest_node([]);
        equal(retval, null, "find_highest_node() returned null on empty list");
        retval = plf.LevelNode.find_highest_node([tn1, tn2]);
        equal(retval, tn2, "find_highest_node() returned test node with highest branch value");
    });

    test("lowest node find test", function() {
        tn1.branch = 1;
        tn2.branch = 2;
        retval = plf.LevelNode.find_lowest_node([]);
        equal(retval, null, "find_lowest_node() returned null on empty list");
        retval = plf.LevelNode.find_lowest_node([tn1, tn2]);
        equal(retval, tn1, "find_lowest_node() returned test node with lowest branch value");
    });

    test("node depth comparison test", function() {
        retval = plf.LevelNode.compare_depths([]);
        equal(retval, true, "compare_depths() successfully returned true on empty list");
        tn1.depth = 1;
        tn2.depth = 2;
        retval = plf.LevelNode.compare_depths([tn1, tn2]);
        equal(retval, false, "compare_depths() successfully returned false");
        retval = plf.LevelNode.compare_depths([tn1, tn1]);
        equal(retval, true, "compare_depths() successfully returned true");
        retval = plf.LevelNode.compare_depths([tn1, tn1], 2);
        equal(retval, false, "compare_depths() successfully returned false");
        retval = plf.LevelNode.compare_depths([tn2, tn2], 2);
        equal(retval, true, "compare_depths() successfully returned true");
    });

    test("branch node comparison test", function() {
        retval = plf.LevelNode.compare_branch_nodes([]);
        equal(retval, true, "compare_branch_nodes() successfully returned true on empty list");
        tn1.branch_node = tn3;
        tn2.branch_node = tn3;
        retval = plf.LevelNode.compare_branch_nodes([tn1, tn2]);
        equal(retval, true, "compare_branch_nodes() successfully returned true when branch node was the same");
        tn2.branch_node = tn1;
        retval = plf.LevelNode.compare_branch_nodes([tn1, tn2]);
        equal(retval, false, "compare_branch_nodes() successfully returned false when branch node was different");
        retval = plf.LevelNode.compare_branch_nodes([tn1, tn2], tn2);
        equal(retval, false, "compare_branch_nodes() successfully returned false when branch node was not the specified one");
        retval = plf.LevelNode.compare_branch_nodes([tn1, tn3], tn3);
        equal(retval, true, "compare_branch_nodes() successfully returned true when branch node was the specified one");
    });

    test("set_id() function test", function() {
        tn1.set_id(5);
        equal(tn1.id, 5, "set_id() method successfully changed the property");
    });

    test("node parent set test", function() {
        tn1.set_parents([tn3]);
        deepEqual(tn1.parents, [tn3], "set_parents() method successfully changed the property");
    });
    
    test("node child set test", function() {
        tn3.set_children([tn1]);
        deepEqual(tn3.children, [tn1], "set_children() method successfully changed the property");
    });

    test("node parent addition test", function() {
        tn1.add_parent(tn2);
        tn1.add_parent(tn3);
        deepEqual(tn1.parents, [tn3, tn2], "add_parents() method successfully changed the property");
    });

    test("node child addition test", function() {
        tn2.add_child(tn1);
        deepEqual(tn2.children, [tn1], "add_child() method successfully changed the property");
    });

    test("node parent removal test", function() {
        tn1.add_parent(tn2);
        tn1.add_parent(tn3);
        tn1.remove_parent(tn2);
        deepEqual(tn1.parents, [tn3], "remove_parent() method successfully changed the property");
    });

    test("node child removal test", function() {
        tn2.remove_child(tn1);
        deepEqual(tn2.children, [], "remove_child() method successfully changed the property");
    });

    test("node relation creation test", function() {
        tn1.create_relation(tn2);
        deepEqual(tn1.parents, [tn2], "create_relation() method successfully changed the property");
        deepEqual(tn2.children, [tn1], "child's create_relation() method successfully changed parent's property");
    });

    test("node child relations creation test", function() {
        tn1.create_child_relations([tn2, tn3]);
        deepEqual(tn1.parents, [tn2, tn3], "create_child_relations() method successfully changed the property");
        deepEqual(tn2.children, [tn1], "create_child_relations() method successfully changed the parent's property");
        deepEqual(tn3.children, [tn1], "create_child_relations() method successfully changed the parent's property");
    });

    test("node parent relations creation test", function() {
        tn1.create_parent_relations([tn2, tn3]);
        deepEqual(tn2.parents, [tn1], "create_parent_relations() method successfully changed the child's property");
        deepEqual(tn3.parents, [tn1], "create_parent_relations() method successfully changed the child's property");
        deepEqual(tn1.children, [tn2, tn3], "create_child_relations() method successfully changed the property");
    });

    test("node relation clearing test", function() {
        tn1.create_relation(tn2);
        deepEqual(tn1.parents, [tn2], "create_relation() successful");
        tn1.clear_relation(tn2);
        deepEqual(tn1.parents, [], "clear_relation() successfully cleared the child's property");
        deepEqual(tn2.children, [], "clear_relation() successfully cleared the parent's property");
    });

    test("node parent relations clearing test", function() {
        tn1.create_child_relations([tn2, tn3]);
        deepEqual(tn1.parents, [tn2, tn3], "create_child_relations() method successfully changed the property");
        tn1.clear_parent_relations();
        deepEqual(tn1.parents, [], "clear_parent_relations() method successfully changed the property");
        deepEqual(tn2.children, [], "clear_parent_relations() method successfully changed the parent's property");
        deepEqual(tn3.children, [], "clear_parent_relations() method successfully changed the parent's property");        
    });

    test("node child relations clearing test", function() {
        tn1.create_parent_relations([tn2, tn3]);
        deepEqual(tn1.children, [tn2, tn3], "create_parent_relations() method successfully changed the property");
        tn1.clear_child_relations();
        deepEqual(tn1.children, [], "clear_parent_relations() method successfully changed the property");
        deepEqual(tn2.parents, [], "clear_parent_relations() method successfully changed the parent's property");
        deepEqual(tn3.parents, [], "clear_parent_relations() method successfully changed the parent's property");        
    });

    test("node relations clearing test", function() {
        tn1.create_relation(tn2);
        deepEqual(tn1.parents, [tn2], "create_relation() method successfully changed the property");
        tn3.create_relation(tn1);
        deepEqual(tn1.children, [tn3], "create_relation() method successfully changed the property");
        tn1.clear_relations();
        deepEqual(tn1.parents, [], "clear_relations() method successfully removed the parents");
        deepEqual(tn1.children, [], "clear_relations() method successfully removed the children");
    });

    test("node insertion test", function() {
        tn1.create_relation(tn3);
        deepEqual(tn1.parents, [tn3], "create_relation() method successfully changed the property");
        tn2.insert_between(tn3, tn1);
        deepEqual(tn1.parents, [tn2], "insert_between() successfully changed the child's property");
        deepEqual(tn3.children, [tn2], "insert_between() successfully changed the parent's property");
        deepEqual(tn2.parents, [tn3], "insert_between() successfully changed the node's parents property");
        deepEqual(tn2.children, [tn1], "insert_between() successfully changed the node's children property");
    });

    test("node relatives clearing test", function() {
        tn1.create_relation(tn2);
        deepEqual(tn1.parents, [tn2], "create_relation() method successfully changed the property");
        tn3.create_relation(tn1);
        deepEqual(tn1.children, [tn3], "create_relation() method successfully changed the property");
        tn1.clear_relatives();
        equal(tn1.parents.length, 0, "clear_relatives() successfully changed the length of the parents property");
        equal(tn1.children.length, 0, "clear_relatives() successfully changed the length of the children property");
    });

    test("node child counting test", function() {
        equal(tn1.get_child_count(), 0, "get_child_count() successfully returned 0");
        tn1.create_parent_relations([tn2, tn3]);
        deepEqual(tn1.children, [tn2, tn3], "create_parent_relations() method successfully changed the property");
        equal(tn1.get_child_count(), 2, "get_child_count() successfully returned 2");
    });

    test("node parent counting test", function() {
        equal(tn1.get_parent_count(), 0, "get_parent_count() successfully returned 0");
        tn1.create_child_relations([tn2, tn3]);
        deepEqual(tn1.parents, [tn2, tn3], "create_child_relations() successfully changed the property");
        equal(tn1.get_parent_count(), 2, "get_parent_count() successfully returned 2");
    });

    test("node highest parent get test", function() {
        equal(tn1.get_highest_parent(), null, "method returned null");
        tn1.create_child_relations([tn2, tn3]);
        tn2.branch = 5;
        tn3.branch = 10;
        deepEqual(tn1.get_highest_parent(), tn3, "method returned the highest parent");
    });

    test("node lowest parent get test", function() {
        equal(tn1.get_lowest_parent(), null, "method returned null");
        tn1.create_child_relations([tn2, tn3]);
        tn2.branch = 5;
        tn3.branch = 10;
        deepEqual(tn1.get_lowest_parent(), tn2, "method returned the lowest parent");
    });

    test("node highest child get test", function() {
        equal(tn1.get_highest_child(), null, "method returned null");
        tn1.create_parent_relations([tn2, tn3]);
        tn2.branch = 5;
        tn3.branch = 10;
        deepEqual(tn1.get_highest_child(), tn3, "method returned the highest child");
    });

    test("node lowest child get test", function() {
        equal(tn1.get_lowest_child(), null, "method returned null");
        tn1.create_parent_relations([tn2, tn3]);
        tn2.branch = 5;
        tn3.branch = 10;
        deepEqual(tn1.get_lowest_child(), tn2, "method returned the lowest child");
    });

    test("tree coords set test", function() {
        tn1.set_tree_coords(2, 3, tn2);
        equal(tn1.depth, 2, "method set depth correctly");
        equal(tn1.branch, 3, "method set branch correctly");
        deepEqual(tn1.branch_node, tn2, "method set branch node correctly");
    });

    test("default child node coordinate get test", function() {
        tn1.set_tree_coords(2, 3, tn2);
        var nc = tn1.get_def_child_coords();
        equal(nc.depth, 3, "method returned correct depth");
        equal(nc.branch, 3, "method returned correct branch");
        deepEqual(nc.branch_node, tn2, "method returned correct branch node");
    });

    test("coord clone test", function() {
        tn1.set_tree_coords(2, 3, tn2);
        var nc = new plf.NodeCoords(null, null, null);
        tn1.clone_def_child_coords(nc);
        equal(nc.depth, 3, "method set the correct depth");
        equal(nc.branch, 3, "method set the correct branch");
        deepEqual(nc.branch_node, tn2, "method returned correct branch node");        
    });

    test("child find test", function() {
        tn1.create_parent_relations([tn2, tn3]);
        deepEqual(tn1.has_child(tn1), false, "method correctly returned false on a non-child node");
        deepEqual(tn1.has_child(tn2), true, "method correctly returned true on a child node");
        deepEqual(tn1.has_child(tn3), true, "method correctly returned true on a child node");
    });
    
    test("partner get test", function() {
        deepEqual(tn1.get_partners(), [], "method correctly returned an empty array");
        tn1.create_parent_relations([tn2]);
        deepEqual(tn1.get_partners(), [], "method correctly returned an empty array");
        tn3.create_parent_relations([tn2]);
        deepEqual(tn1.get_partners(), [tn3], "method correctly returned a partner node");
    });

    // TODO: connect_branches_from test

    test("last descendant get test", function() {
        tn1.create_parent_relations([tn2]);
        tn2.create_parent_relations([tn3]);
        deepEqual(tn1.get_last_desc_on_branch(), tn3, "method returned the correct node");
    });

    test("lower descendant existance test", function() {
        deepEqual(tn1.has_lower_descendant(), false, "method correctly returned false");
        tn1.create_parent_relations([tn2]);
        tn1.branch = 1;
        deepEqual(tn1.has_lower_descendant(), true, "method correctly returned true");
    });
})();

/*
 * Test the nodetree module.
 */
(function() {
    var node1;
    var node2;
    var node3;
    var ntree;

    module("nodetree.js", {
        setup: function() {
            node1 = new plf.LevelNode();
            node2 = new plf.LevelNode();
            node3 = new plf.LevelNode();
            ntree = new plf.NodeTree("test_tree", node1);
        }
    });

    test("initialization test", function() {
        ok(node1, "test node 1 successfully created");
        ok(node2, "test node 2 successfully created");
        ok(node3, "test node 3 successfully created");
        ok(ntree, "test tree successfully created");
    });

    test("set root test", function() {
        ntree.set_root(node2);
        deepEqual(ntree.root, node2, "root node set correctly");
        deepEqual(ntree.root.id, 0, "root node id set correctly");
    });

    test("node array get test", function() {
        deepEqual(ntree.get_nodes(), [node1], "method returned the correct array");
    });

    test("node count get test", function() {
        deepEqual(ntree.get_node_count(), 1, "method returned the correct count");
    });

    test("node get by id test", function() {
        deepEqual(ntree.get_node_by_id(1), null, "method returned null correctly");
        deepEqual(ntree.get_node_by_id(0), node1, "method returned the correct node");
    });

    test("node get by coords test", function() {
        deepEqual(ntree.get_node_by_coords(1, 0), null, "method returned null correctly");
        deepEqual(ntree.get_node_by_coords(0, 1), null, "method returned null correctly");
        deepEqual(ntree.get_node_by_coords(1, 1), null, "method returned null correctly");
        deepEqual(ntree.get_node_by_coords(0, 0), node1, "method returned the correct node");
    });

    test("node get by element test", function() {
        var elem = new plf.Element();
        deepEqual(ntree.get_node_by_element(elem), null, "method returned null correctly");
        node1.element = elem;
        deepEqual(ntree.get_node_by_element(elem), node1, "method returned the correct node");
    });

    test("last leaf node get test", function() {
        deepEqual(ntree.get_last_leaf(), node1, "method returned the correct node");
    });

    test("tree depth get test", function() {
        deepEqual(ntree.get_depth(), 1, "method returned the correct value");
    });

    test("tree height get test", function() {
        deepEqual(ntree.get_height(), 1, "method returned the correct value");
    });

    test("node ownership test", function() {
        deepEqual(ntree.has_node(node2), false, "method returned false correctly");
        deepEqual(ntree.has_node(node1), true, "method returned true correctly");
    });

    test("node id ownership test", function() {
        deepEqual(ntree.has_node_with_id(0), true, "method returned true correctly");
        deepEqual(ntree.has_node_with_id(1), false, "method returned false correctly");
    });

    test("node coords ownership test", function() {
        deepEqual(ntree.has_node_with_coords(0, 0), true, "method returned true correctly");
        deepEqual(ntree.has_node_with_coords(1, 1), false, "method returned false correctly");
    });

    test("node between coords ownership test", function() {
        deepEqual(ntree.has_node_between_coords(0, -1, 1), true, "method returned true correctly");
    });

    test("potential partner find test", function() {
        deepEqual(ntree.find_potential_partners(node1), [], "method returned an empty list");
    });

    test("child branch move test", function() {
        var retval = ntree.move_child_branches(node1, 2, 0);
        deepEqual(retval, 2, "method returned the correct value");
        deepEqual(ntree.get_nodes()[0].branch, 2, "method successfully changed the node's branch value");
    });

    test("branch lowering test", function() {
        var retval = ntree.allows_branch_lowering(node1, 0);
        deepEqual(retval, false, "method returned the correct value");
        retval = ntree.allows_branch_lowering(node1, -1);
        deepEqual(retval, true, "method returned the correct value");
    });

    test("ancestor branch move test", function() {
        ntree.move_ancestor_branches(node1, 2, 0);
        deepEqual(ntree.get_nodes()[0].branch, 0, "method didn't touch the node's branch value");
    });

    test("branch stretch test", function() {
        ntree.stretch_branch(node1);
        deepEqual(ntree.get_nodes()[0].depth, 1, "method changed the node's branch value");
        ntree.stretch_branch(node1);
        deepEqual(ntree.get_nodes()[0].depth, 2, "method changed the node's branch value");
    });

    /*test("branches' stretch test", function() {
        ntree.stretch_branches(node1, []);
        deepEqual(ntree.get_nodes()[0].depth, 1, "method didn't add more nodes");
    });*/

    test("node map to id test", function() {
        ntree.map_node_to_id(node1);
        deepEqual(ntree.nodes["1"], node1, "method mapped the node correctly");
    });

    test("allow node with coords test", function() {
        deepEqual(ntree.allows_node_with_coords(new plf.NodeCoords(0, 0)), true, "method returned the correct value");
        deepEqual(ntree.allows_node_with_coords(new plf.NodeCoords(0, 1)), false, "method returned the correct value");
        deepEqual(ntree.allows_node_with_coords(new plf.NodeCoords(1, 0)), true, "method returned the correct value");
        //deepEqual(ntree.allows_node_with_coords(new plf.NodeCoords(1, 1)), false, "method returned the correct value");
    });

    /*test("allow as parents test", function() {
        var retval = ntree.allows_as_parents([node1], new plf.NodeCoords(0, 0));
        deepEqual(retval, true, "method returned the correct value");
    });*/

    test("allow another child test", function() {
        deepEqual(ntree.allows_another_child(node1, new plf.NodeCoords(0, 0)), false, "method returned the correct value");
    });

    test("allow leaf test", function() {
        deepEqual(ntree.allows_leaf([node1], new plf.NodeCoords(0, 0)), true, "method returned the correct value");
        deepEqual(ntree.allows_leaf([node1], new plf.NodeCoords(0, 1)), true, "method returned the correct value");
        deepEqual(ntree.allows_leaf([node1], new plf.NodeCoords(1, 0)), true, "method returned the correct value");
        deepEqual(ntree.allows_leaf([node1], new plf.NodeCoords(1, 1)), true, "method returned the correct value");
    });

    test("leaf add test", function() {
        var retval = ntree.add_leaf(node2, [node1]);
        deepEqual(retval, true, "method correctly added a leaf node");
    });

    test("allow inner test", function() {
        deepEqual(ntree.allows_inner(node1, node2), false, "method correctly returned false");
    });

    test("add inner test", function() {
        ntree.add_leaf(node2, [node1]);
        deepEqual(ntree.add_inner(node3, node1, node2, []), true, "method correctly returned true");
        deepEqual(ntree.add_inner(undefined, node1, node2, []), false, "method correctly returned false");
    });

    test("remove id mapping test", function() {
        deepEqual(node1.id, 0, "test node id is 0");
        deepEqual(ntree.nodes[0], node1, "test node found in id hashmap");
        ntree.remove_id_mapping(node1);
        deepEqual(node1.id, null, "test node correctly set to null");
        deepEqual(ntree.nodes[0], undefined, "test node correctly removed from id hashmap");
    });

    test("remove child branches test", function() {
        ntree.add_leaf(node2, [node1]);
        deepEqual(node1.get_child_count(), 1, "child correctly added");
        ntree.remove_child_branches(node1);
        deepEqual(node1.get_child_count(), 0, "children correctly removed");
    });

    /*test("remove higher child branches test", function() {
        node1.branch = 0;
        node2.branch = 1;
        node3.branch = 0;
        ntree.add_leaf(node2, [node1]);
        ntree.add_leaf(node3, [node1]);
        deepEqual(node1.get_child_count(), 2, "children correctly added");
        ntree.remove_higher_child_branches(node1);
        deepEqual(node1.get_child_count(), 1, "higher branched children correctly removed");
    });*/

    test("compress branch test", function() {
        ntree.add_leaf(node2, [node1]);
        node1.depth = 1;
        node2.depth = 1;
        deepEqual(node1.depth, 1, "node depth correctly set");
        deepEqual(node2.depth, 1, "node child depth correctly set");
        ntree.compress_branch(node1);
        deepEqual(node1.depth, 0, "node depth correctly lowered");
        deepEqual(node2.depth, 0, "node child depth correctly lowered");
    });

    /*test("compress branches test", function() {
        var b_removed = [];
    });*/
    
    test("allows removing test", function() {
        ntree.add_leaf(node2, [node1]);
        deepEqual(ntree.allows_removing(node1), false, "node 1 (root) unremovable");
        deepEqual(ntree.allows_removing(node2), true, "node 2 (in tree) removable");
        deepEqual(ntree.allows_removing(node3), false, "node 3 (not in tree) unremovable");
    });

    test("remove node test", function() {
        ntree.add_leaf(node2, [node1]);
        ntree.add_leaf(node3, [node2]);
        ntree.remove_node(node2);
        deepEqual(ntree.has_node(node1), true, "node 1 correctly found in tree");
        deepEqual(ntree.has_node(node2), false, "node 2 correctly not found in tree");
        deepEqual(ntree.has_node(node3), true, "node 3 correctly found in tree");
    });

    /*
    test("allows connecting test", function() {
        deepEqual(ntree.allows_connecting(node1, node2), false, "correctly doesn't allow connection of node1 and node2");
    });

    test("connect nodes test", function() {
        deepEqual(ntree.connect_nodes(node1, node2), false, "correctly didn't connect node1 and node2");
    });
    */

    test("allows disconnecting test", function() {
        ntree.add_leaf(node2, [node1]);
        ntree.add_leaf(node3, [node2]);
        node3.branch = -1;
        deepEqual(ntree.allows_disconnecting(node1, node2), false, "correctly doesn't allow disconnection of node1 and node2");
        deepEqual(ntree.allows_disconnecting(node2, node3), true, "correctly allows disconnection of node2 and node3");
    });

    test("disconnect nodes test", function() {
        ntree.add_leaf(node2, [node1]);
        ntree.add_leaf(node3, [node2]);
        node3.branch = -1;
        deepEqual(ntree.disconnect_nodes(node1, node2), false, "correctly didn't disconnect node1 and node2");
        deepEqual(ntree.disconnect_nodes(node2, node3), true, "correctly disconnected of node2 and node3");
    });

    test("remove node by id test", function() {
        ntree.add_leaf(node2, [node1]);
        deepEqual(ntree.has_node(node2), true, "node 2 correctly added to tree");
        ntree.remove_node_by_id(node2.id);
        deepEqual(ntree.has_node(node2), false, "node 2 correctly removed from the tree by id");
    });
})();

/*
 * Test the element module.
 */
(function() {
    var dummy_levelrunner = {};
    // For push random test
    dummy_levelrunner.get_next_test_input = function() {
        return null;
    };
    // For sys exit test
    var finish_state;
    dummy_levelrunner.finish_test = function(p1) {
        finish_state = p1;
    };
    var node1;
    var node2;
    var ntree1;
    var ntree2;
    var lvl;
    var inv;
    var plr;
    var el;

    module("element.js", {
        setup: function() {
            node1 = new plf.LevelNode();
            node2 = new plf.LevelNode();
            ntree1 = new plf.NodeTree("tree_1", node1);
            ntree2 = new plf.NodeTree("tree_2", node2);
            lvl = new plf.BuildLevel([ntree1, ntree2], []);
            var new_node = node1;
            for (var i = 0; i < 5; i++) {
                lvl.add_platform(ntree1, new_node);
                new_node = new_node.get_lowest_child();
            }

            inv = new plf.Inventory();
            plr = new plf.Player(lvl, "tree_1", node1.id, inv);

            el = new plf.Element();

            finish_state = undefined;
        }
    });

    test("initialization test", function() {
        deepEqual(dummy_levelrunner.get_next_test_input(), null, "dummy testrunner function returned correct value");
    });

    test("pass exec test", function() {
        deepEqual([plr.position.tree, plr.position.id], ["tree_1", 0], "player started at correct position");
        el.type = plf.PASS;
        el.colors = el.get_default_colors();
        el.exec(plr, dummy_levelrunner);
        deepEqual([plr.position.tree, plr.position.id], ["tree_1", 1], "player moved to correct position");
    });

    test("for jump exec test", function() {
        plr.update_position(ntree1.get_last_leaf().id);
        deepEqual([plr.position.tree, plr.position.id], ["tree_1", 5], "player started at correct position");
        var stmt = new plf.Element(plf.FOR_STMT);
        node1.element = stmt;
        stmt.param = {};
        stmt.param.iterator = ["a", "b"];
        el.type = plf.FOR_JUMP;
        el.param = stmt;
        el.colors = el.get_default_colors();
        el.exec(plr, dummy_levelrunner);
        deepEqual([plr.position.tree, plr.position.id], ["tree_1", 0], "player moved to correct position");
        
    });

    test("for statement exec test", function() {
        el.type = plf.FOR_STMT;
        el.param = {};
        el.param.iterator = ["a", "b"];
        el.colors = el.get_default_colors();
        el.exec(plr, dummy_levelrunner);
        deepEqual(el.param.iterator, ["a"], "iterator popped correctly");
        deepEqual(plr.get_inventory_slot(plf.RED), ["b"], "inventory updated correctly");
    });

    test("push exec test", function() {
        el.type = plf.PUSH;
        el.param = "item string";
        el.colors = el.get_default_colors();
        el.exec(plr, dummy_levelrunner);
        deepEqual(plr.get_inventory_slot(plf.RED), ["item string"], "inventory updated correctly");
    });

    test("pop exec test", function() {
        el.type = plf.POP;
        el.param = null;
        el.colors = el.get_default_colors();
        plr.set_inventory_slot(plf.RED, ["item"]);
        el.exec(plr, dummy_levelrunner);
        deepEqual(plr.get_inventory_slot(plf.RED), [], "red inventory transformed correctly");
        deepEqual(plr.get_inventory_slot(plf.BLUE), ["item"], "blue inventory transformed correctly");
    });

    test("delete exec test", function() {
        el.type = plf.DELETE;
        el.param = null;
        el.colors = el.get_default_colors();
        plr.set_inventory_slot(plf.RED, ["item"]);
        el.exec(plr, dummy_levelrunner);
        deepEqual(plr.get_inventory_slot(plf.RED), [], "red inventory slot deleted correctly");
    });

    test("assign exec test", function() {
        el.type = plf.ASSIGN;
        el.param = ["item 1", "item 2"];
        el.colors = el.get_default_colors();
        el.exec(plr, dummy_levelrunner);
        deepEqual(plr.get_inventory_slot(plf.RED), ["item 1", "item 2"], "red inventory slot assigned correctly");
    });

    test("push random exec test", function() {
        el.type = plf.PUSH_RANDOM;
        el.param = ["a", "b"];
        el.colors = el.get_default_colors();
        el.exec(plr, dummy_levelrunner);
        var retval = plr.pop_from_inventory(el.colors[plf.ELEMENT]);
        ok(retval == "a" || retval == "b", "function worked correctly");
    });

    test("function call exec test", function() {
        el.type = plf.FUNC_CALL;
        el.param = "tree_2";
        el.colors = el.get_default_colors();
        el.exec(plr, dummy_levelrunner);
        deepEqual(plr.position.tree, "tree_2", "player correctly transferred to tree 2");
    });

    test("return statement exec test", function() {
        el.type = plf.RETURN_STMT;
        el.param = null;
        el.colors = el.get_default_colors();
        plr.push_callstack();
        plr.update_position(0, "tree_2");
        node1.element = {};
        node1.element.colors = {return_val: plf.BLUE};
        el.exec(plr, dummy_levelrunner);
        deepEqual(plr.position.tree, "tree_1", "player correctly returned to tree 1");
    });

    test("conditional exec test", function() {
        el.type = plf.CONDITIONAL;
        el.param = [{itemname: 1}, {otheritemname: 1}];
        el.colors = el.get_default_colors();
        el.exec(plr, dummy_levelrunner);
        deepEqual(plr.position.id, 1, "player correctly moved to next node");
    });

    test("sys exit exec test", function() {
        deepEqual(finish_state, undefined, "finish state initialized correctly");
        el.type = plf.SYS_EXIT;
        el.param = ["a", "b"];
        el.colors = el.get_default_colors();
        var invobj = {};
        invobj[el.colors[plf.ELEMENT]] = ["a", "b"];
        plr.set_inventory(invobj);
        el.exec(plr, dummy_levelrunner);
        deepEqual(finish_state, true, "function worked correctly");
    });
})();

/*
 * Test the level module.
 */
(function() {
    var node1;
    var node2;
    var ntree1;
    var ntree2;
    var lvl;
    var rlvl;
    var blvl;

    module("level.js", {
        setup: function() {
            node1 = new plf.LevelNode();
            node2 = new plf.LevelNode();
            ntree1 = new plf.NodeTree("tree_1", node1);
            ntree2 = new plf.NodeTree("tree_2", node2);
            lvl = new plf.Level([ntree1, ntree2]);
            rlvl = new plf.ReadLevel([ntree1, ntree2]);
            blvl = new plf.BuildLevel([ntree1, ntree2], []);
        }
    });

    test("initialization test", function() {
        ok(node1, "test node for tree_1 successfully created");
        ok(node2, "test node for tree_2 successfully created");
        ok(ntree1, "test tree 1 successfully created");
        ok(ntree2, "test tree 2 successfully created");
        ok(lvl, "test level successfully created");
        ok(rlvl, "test read level successfully created");
        ok(blvl, "test build level successfully created");
    });

    // The base Level type tests

    test("has tree with name test", function() {
        deepEqual(lvl.has_tree_with_name("tree_1"), true, "tree_1 named tree correctly found");
        deepEqual(lvl.has_tree_with_name("tree_2"), true, "tree_2 named tree correctly found");
        deepEqual(lvl.has_tree_with_name("tree_3"), false, "tree_3 named tree correctly not found");
    });

    test("get tree by name test", function() {
        deepEqual(lvl.get_tree_by_name("tree_1"), ntree1, "function returned the correct value");
        deepEqual(lvl.get_tree_by_name("tree_2"), ntree2, "function returned the correct value");
        deepEqual(lvl.get_tree_by_name("tree_3"), null, "function returned the correct value");
    });

    test("get node test", function() {
        deepEqual(lvl.get_node("tree_1", 0), node1, "function returned the correct node");
        deepEqual(lvl.get_node("tree_2", 0), node2, "function returned the correct node");
        deepEqual(lvl.get_node("tree_1", 10), null, "function correctly returned null");
        deepEqual(lvl.get_node("tree_3", 0), null, "function correctly returned null");
    });

    test("get tree and node test", function() {
        deepEqual(lvl.get_tree_and_node("tree_1", 0), {tree: ntree1, node: node1}, "function returned the correct object");
        deepEqual(lvl.get_tree_and_node("tree_2", 0), {tree: ntree2, node: node2}, "function returned the correct object");
        deepEqual(lvl.get_tree_and_node("tree_3", 0), {tree: null, node: null}, "function returned the correct object");
        deepEqual(lvl.get_tree_and_node("tree_1", 1), {tree: ntree1, node: null}, "function returned the correct object");
    });

    test("has tree test", function() {
        deepEqual(lvl.has_tree(ntree1), true, "function returned the correct boolean value");
        deepEqual(lvl.has_tree(ntree2), true, "function returned the correct boolean value");
        deepEqual(lvl.has_tree(null), false, "function returned the correct boolean value");
    });

    test("has tree with node test", function() {
        deepEqual(lvl.has_tree_with_node(ntree1, node1), true, "function returned the correct boolean value");
        deepEqual(lvl.has_tree_with_node(ntree2, node2), true, "function returned the correct boolean value");
        deepEqual(lvl.has_tree_with_node(ntree1, null), false, "function returned the correct boolean value");
        deepEqual(lvl.has_tree_with_node(null, node1), false, "function returned the correct boolean value");
    });

    // The ReadLevel type tests (NOTE: ReadLevel not fully implemented)

    /*
    test("get goal count test", function() {
        deepEqual(rlvl.get_goal_count(), 0, "function returned the correct value");
    });

    test("set finish condition test", function() {
        rlvl.set_finish_condition("tree_1", 0, ???);
        deepEqual(node1.element.param, ???, "function set the correct value");
    });
    */

    // The BuildLevel type tests

    test("get allowed actions test", function() {
        deepEqual(blvl.get_allowed_actions("tree_1", 0), ["add"]);
        deepEqual(blvl.get_allowed_actions("tree_2", 0), ["add"]);
    });

    test("execute action test", function() {
        deepEqual(blvl.execute_action("tree_1", 0, "add"), true, "function returned the correct value");
        deepEqual(blvl.execute_action("tree_1", 0, "remove"), false, "function returned the correct value");
    });

    test("get disabled elements test", function() {
        deepEqual(blvl.get_disabled_elements("tree_1", 0), [], "function returned the correct value");
    });

    test("allows adding test", function() {
        deepEqual(blvl.allows_adding(ntree1, node1), true, "function returned the correct value");
    });

    test("allows removing test", function() {
        deepEqual(blvl.allows_removing(ntree1, node1), false, "function returned the correct value");
    });

    test("allows connecting test", function() {
        deepEqual(blvl.allows_connecting(ntree1, node1), false, "function returned the correct value");
    });

    test("allows disconnecting test", function() {
        deepEqual(blvl.allows_disconnecting(ntree1, node1), false, "function returned the correct value");
    });

    /*test("allows for jump test", function() {
      deepEqual();
    });*/

    test("allows element test", function() {
        deepEqual(blvl.allows_element(ntree1, node1, new plf.Element(plf.PASS, null)), false, "function returned the correct value");
    });

    test("allows clearing test", function() {
        deepEqual(blvl.allows_clearing(ntree1, node1), false, "function returned the correct value");
    });

    test("allows lifting test", function() {
        deepEqual(blvl.allows_lifting(ntree1, node1), false, "function returned the correct value");
    });

    test("add platform test", function() {
        ok(node1.children[0] === undefined);
        deepEqual(blvl.add_platform(ntree1, node1), true, "function returned the correct value");
        ok(node1.children[0]);
    });

    test("remove platform test", function() {
        blvl.add_platform(ntree1, node1);
        var new_node = node1.children[0];
        deepEqual(blvl.remove_platform(ntree1, new_node), true, "function returned the correct value");
        deepEqual(node1.children[0], undefined, "function correctly removed the child");
    });

    test("connect platform test", function() {
        blvl.add_platform(ntree1, node1);
        var new_node = node1.children[0];
        deepEqual(blvl.connect_platform(ntree1, new_node), false, "function returned the correct value");
    });

    test("disconnect platform test", function() {
        blvl.add_platform(ntree1, node1);
        var new_node = node1.children[0];
        deepEqual(blvl.disconnect_platform(ntree1, new_node), false, "function returned the correct value");
    });

    test("clear conditional test", function() {
        deepEqual(blvl.clear_conditional(ntree1, node1), false, "function returned the correct value");
    });

    test("clear for jump pair test", function() {
        blvl.add_platform(ntree1, node1);
        var new_node_1 = node1.children[0];
        blvl.add_platform(ntree1, new_node_1);
        var new_node_2 = new_node_1.children[0];
        ok(new_node_2);
        var nodes = [new_node_2];
        new_node_1.element = new plf.Element(plf.FOR_STMT);
        new_node_2.element = new plf.Element(plf.FOR_JUMP, new_node_1.element);
        blvl.clear_for_jump_pair(new_node_1, nodes);
        deepEqual(new_node_2.element.type, plf.PASS, "function set the correct value for the second node");
    });

    test("clear for statement test", function() {
        blvl.add_platform(ntree1, node1);
        var new_node_1 = node1.children[0];
        blvl.add_platform(ntree1, new_node_1);
        var new_node_2 = new_node_1.children[0];
        ok(new_node_2);
        var nodes = [new_node_2];
        new_node_1.element = new plf.Element(plf.FOR_STMT);
        new_node_2.element = new plf.Element(plf.FOR_JUMP, new_node_1.element);
        blvl.clear_for_stmt(ntree1, new_node_1);
        deepEqual(new_node_1.element.type, plf.PASS, "function set the correct value for first node");
        deepEqual(new_node_2.element.type, plf.PASS, "function set the correct value for second node");
    });

    test("clear element test", function() {
        blvl.add_platform(ntree1, node1);
        var new_node_1 = node1.children[0];
        new_node_1.element = new plf.Element(plf.PUSH_RANDOM);
        deepEqual(new_node_1.element.type, plf.PUSH_RANDOM, "test element correctly set");
        deepEqual(blvl.clear_element(ntree1, new_node_1), true, "function returned the correct value");
        deepEqual(new_node_1.element.type, plf.PASS, "function set the correct value");
    });

    test("place element test", function() {
        blvl.add_platform(ntree1, node1);
        var new_node_1 = node1.children[0];
        var elem = new plf.Element(plf.PUSH_RANDOM);
        deepEqual(blvl.place_element("tree_1", new_node_1.id, elem), false, "function returned the correct value");
    });

    test("lift branches test", function() {
        blvl.add_platform(ntree1, node1);
        var new_node_1 = node1.children[0];
        deepEqual(blvl.lift_branches(ntree1, new_node_1), false, "function returned the correct value");
    });
})();

/*
 * Test the player module.
 */
(function() {
    // The level needed by the player
    var node1;
    var node2;
    var ntree1;
    var ntree2;
    var lvl;

    var inv;
    var plr;

    module("player.js", {
        setup: function() {
            node1 = new plf.LevelNode();
            node2 = new plf.LevelNode();
            ntree1 = new plf.NodeTree("tree_1", node1);
            ntree2 = new plf.NodeTree("tree_2", node2);
            lvl = new plf.BuildLevel([ntree1, ntree2], []);

            inv = new plf.Inventory();
            plr = new plf.Player(lvl, "tree_1", 0, inv);
        }
    });

    test("initialization test", function() {
        ok(node1, "test node for tree_1 successfully created");
        ok(node2, "test node for tree_2 successfully created");
        ok(ntree1, "test tree 1 successfully created");
        ok(ntree2, "test tree 2 successfully created");
        ok(lvl, "test level successfully created");

        ok(inv, "test inventory successfully created");
        ok(plr, "test player successfully created");
    });

    test("interact test", function() {
        lvl.add_platform(ntree1, node1);
        ok(node1.children[0], "new node added");
        deepEqual(plr.position, {level: lvl, tree: "tree_1", id: 0}, "player at the start of tree");
        plr.interact(null);
        deepEqual(plr.position, {level: lvl, tree: "tree_1", id: 1}, "player moved to next node");
    });

    test("get current node test", function() {
        lvl.add_platform(ntree1, node1);
        var new_node = node1.children[0];
        ok(new_node, "new node added");
        deepEqual(plr.get_current_node(), node1, "player at node 1");
        plr.interact(null);
        deepEqual(plr.get_current_node(), new_node, "player at new node");
    });

    test("get current tree test", function() {
        deepEqual(plr.get_current_tree(), ntree1, "player at tree 1");
    });

    test("update position test", function() {
        deepEqual(plr.get_current_node(), node1, "player at node 1");
        plr.update_position(0, "tree_2");
        deepEqual(plr.get_current_node(), node2, "player at node 2");
    });

    test("push callstack test", function() {
        var position = {id: plr.position.id, tree: plr.position.tree};
        var inventory = plr.inventory;
        plr.push_callstack();
        deepEqual(plr.callstack[1], {position: position, inventory: inventory}, "pushed correctly into callstack");
    });

    test("pop callstack test", function() {
        var position = {id: plr.position.id, tree: plr.position.tree};
        var inventory = plr.inventory;
        plr.push_callstack();
        deepEqual(plr.pop_callstack(), {position: position, inventory: inventory}, "callstack correctly popped");
    });

    test("push to inventory test", function() {
        plr.push_to_inventory(plf.RED, "test");
        deepEqual(plr.inventory[plf.RED].contains("test"), true, "item correctly pushed to inventory");
    });

    test("pop from inventory test", function() {
        plr.push_to_inventory(plf.RED, "test");
        deepEqual(plr.inventory[plf.RED].contains("test"), true, "item correctly pushed to inventory");
        deepEqual(plr.pop_from_inventory(plf.BLUE), null, "no item correctly popped from blue inventory");
        deepEqual(plr.pop_from_inventory(plf.RED), "test", "test item correctly popped from red inventory");
    });

    test("set inventory test", function() {
        var test_obj = {test: "abc"};
        plr.set_inventory(test_obj);
        deepEqual(plr.inventory, test_obj, "inventory correctly set");
    });

    test("set inventory slot test", function() {
        var test_items = ["1", "2", "3", "4"];
        plr.set_inventory_slot(plf.RED, test_items);
        deepEqual(plr.inventory[plf.RED], test_items, "inventory slot correctly set");
    });

    test("get inventory slot test", function() {
        var test_items = ["1", "2", "3", "4"];
        plr.set_inventory_slot(plf.RED, test_items);
        deepEqual(plr.get_inventory_slot(plf.RED), test_items, "inventory slot correctly got");
    });

    test("item first in slot test", function() {
        var test_items = ["1", "2", "3", "4"];
        plr.set_inventory_slot(plf.RED, test_items);
        deepEqual(plr.item_first_in_slot("1", plf.RED), false, "function returned the correct boolean value");
        deepEqual(plr.item_first_in_slot("4", plf.RED), true, "function returned the correct boolean value");
    });
})();

/*
 * Test the parser module.
 */
(function() {
    var level_path = "assign.json";
    var level_data;
    var dummy_ui = {};
    dummy_ui.init_level = function(p1, p2) {
        level_data = p1;
    };
    var json1 = [{type:"pass"},
                 {type:"pass"},
                 {type:"conditional",param:["violet"]},
                 [
                     [{type:"pass"},{type:"pass"}],
                     [{type:"pass"},{type:"assign",param:["pink"]}]
                 ],
                 {type:"pass"}];
    var json2 = [{"type":"for_stmt", "param":["pink","greenish"]},
                 {"type":"sys_exit", "param":["pink"]},
                 {"type":"assign", "param":["greenish"]}];

    module("parser.js", {
        setup: function() {
            level_data = undefined;
        }
    });

    test("initialization test", function() {
        ok(dummy_ui.init_level, "dummy ui function found");
    });

    asyncTest("parse level test", function() {
        expect(2);
        plf.parse_level(level_path, dummy_ui);

        setTimeout(function() {
            ok(level_data, "level data found");
            ok(level_data instanceof plf.BuildLevel, "level data correct type");
            start();
        }, 1000);
    });

    test("parse json level test", function() {
        var json = {trees: [json1], elements: json2};
        var level = plf.parse_json_level(json);
        ok(level instanceof plf.BuildLevel, "level data correct type");
    });

    test("parse json elements test", function() {
        var json = json2;
        var parse_obj = plf.parse_json_elements(json);
        deepEqual(parse_obj.for_stmt_count, 2, "found correct number of for statements");
        deepEqual(parse_obj.elements[0].type, plf.FOR_STMT, "found a for statement at right place");
        deepEqual(parse_obj.elements[1].type, plf.SYS_EXIT, "found a sys exit at right place");
        deepEqual(parse_obj.elements[2].type, plf.ASSIGN, "found an assign at right place");
        deepEqual(parse_obj.elements[3].type, plf.FOR_JUMP, "found a for jump at right place");
    });

    test("parse json trees test", function() {
        var json = [json1, json1];
        var trees = plf.parse_json_trees(json, 0);
        deepEqual(trees[0].get_nodes().length, 9, "correct number of nodes in parsed tree");
        deepEqual(trees[0].nodes[3].element.type, plf.CONDITIONAL, "correct type for conditional");
        deepEqual(trees[0].nodes[3].children.length, 2, "correct number of children for conditional");
        deepEqual(trees[0].nodes[8].parents.length, 2, "correct number of parents for the end node");
    });

    test("parse json tree test", function() {
        var json = json1;
        var tree = plf.parse_json_tree(json, "test_name", 0);
        deepEqual(tree.get_nodes().length, 9, "correct number of nodes in parsed tree");
        deepEqual(tree.nodes[3].element.type, plf.CONDITIONAL, "correct type for conditional");
        deepEqual(tree.nodes[3].children.length, 2, "correct number of children for conditional");
        deepEqual(tree.nodes[8].parents.length, 2, "correct number of parents for the end node");
    });

    test("parse json branch test", function() {
        var json = json1;
        var tree = new plf.NodeTree("test_tree");
        var parents = [tree.root];
        var for_stmt_count = 0;
        var for_stmt_stack = [];
        var node = plf.parse_json_branch(json, tree, parents, for_stmt_count, for_stmt_stack);
        deepEqual(tree.get_nodes().length, 9, "correct number of nodes in parsed tree");
        deepEqual(tree.nodes[3].element.type, plf.CONDITIONAL, "correct type for conditional");
        deepEqual(tree.nodes[3].children.length, 2, "correct number of children for conditional");
        deepEqual(tree.nodes[8].parents.length, 2, "correct number of parents for the end node");
    });

    test("parse json node test", function() {
        var json = {type : plf.SYS_EXIT};
        var for_stmt_stack = [];
        var for_stmt_count = 0;
        var parse_obj = plf.parse_json_node(json, for_stmt_stack, for_stmt_count);
        ok(parse_obj.node instanceof plf.LevelNode, "node successfully created");
        ok(parse_obj.node.element instanceof plf.Element, "node element successfully created");
        deepEqual(parse_obj.node.element.type, plf.SYS_EXIT, "element type successfully passed");
    });
})();

/*
 * Test the levelrunner module.
 */
(function() {
    var json1 = [{type:"pass"},
                 {type:"pass"},
                 {type:"conditional",param:["violet"]},
                 [
                     [{type:"pass"},{type:"pass"}],
                     [{type:"pass"},{type:"assign",param:["pink"]}]
                 ],
                 {type:"pass"}];
    var json2 = [{"type":"for_stmt", "param":["pink","greenish"]},
                 {"type":"sys_exit", "param":["pink"]},
                 {"type":"assign", "param":["greenish"]}];
    var json_all = {trees: [json1], elements: json2};
    var tests = [["pink","orange"]];

    var level;
    var runner;

    module("levelrunner.js", {
        setup: function() {
            level = plf.parse_json_level(json_all);
            runner = new plf.LevelRunner(tests, level);
        }
    });

    test("initialization test", function() {
        ok(runner instanceof plf.LevelRunner, "levelrunner object is of correct type");
    });

    test("push tests test", function() {
        runner.push_tests();
        deepEqual(runner.test_stack, runner.tests, "tests pushed into stack");
        deepEqual(runner.success_tests, [], "successful tests emptied");
        deepEqual(runner.failure_tests, [], "failed tests emptied");
    });

    test("pop test test", function() {
        runner.push_tests();
        deepEqual(runner.pop_test(), 0, "function returned correct value");
        deepEqual(runner.test_stack.length, 0, "function modified test stack correctly");
    });

    test("finish test test", function() {
        runner.finish_test(true);
        deepEqual(runner.status_text.text, "Congratulations! Level completed!", "function set the correct win text");
        runner.finish_test(false);
        deepEqual(runner.status_text.text, "1/1 tests failed!", "function set the correct fail text");
    });

    test("is test running test", function() {
        runner.test_running = 1;
        deepEqual(runner.is_test_running(), true, "function returned the correct value");
        runner.test_running = -1;
        deepEqual(runner.is_test_running(), false, "function returned the correct value");
    });

    test("get next test input test", function() {
        runner.test_running = 0;
        deepEqual(runner.get_next_test_input(), "pink", "function returned the correct value");
        deepEqual(runner.get_next_test_input(), "orange", "function returned the correct value");
        runner.test_running = -1;
        deepEqual(runner.get_next_test_input(), null, "function returned the correct value");
    });

    test("is test ready test", function() {
        runner.test_running = -1;
        deepEqual(runner.is_test_ready(), false, "function returned the correct value");
    });

    // TODO: Test the following functions in the gui.js module
    // - runner.player_move(player, gui)
    // - runner.move_to_next(nextX, nextY, player, gui)
})();

/*
 * Test the gui module.
 */
(function() {
    var container = document.createElement("div");
    container.id = "container";
    var canvas;
    var gui;
    var UI = plf.UserInterface;

    module("gui.js", {
        setup: function() {
            canvas = document.createElement("canvas");
            canvas.id = "game_canvas";
            container.appendChild(canvas);
            $(canvas).css({display: "none",
                           margin: "0 auto",
                           border: "1px solid #000"});
            $(canvas).width(1030);
            $(canvas).height(950);

            gui = new plf.UserInterface();
            plf.parse_level("assign.json", gui);
            gui.initialize(canvas);
        },
        teardown: function() {
            container.removeChild(canvas);
            delete gui;
            delete canvas;
        }
    });

    test("initialization test", function() {
        ok(canvas, "dummy canvas successfully created");
        ok(gui instanceof plf.UserInterface, "gui object is of correct type");
        deepEqual(gui.initialized, true, "UserInterface object correctly initialized");
    });

    test("create stage test", function() {
        ok(UI.create_stage(canvas, 1) instanceof createjs.Stage, "function returned the correct value");
    });

    test("load graphics test", function() {
        var gfx = UI.load_graphics("../graph/");
        ok(gfx.level_sprites instanceof createjs.Sprite, "level sprites loaded correctly");
        ok(gfx.menu_sprites instanceof createjs.Sprite, "menu sprites loaded correctly");
    });

    test("get sprite height test", function() {
        deepEqual(UI.get_sprite_height("actions"), 268, "function returned the correct value");
    });

    test("get sprite width test", function() {
        deepEqual(UI.get_sprite_width("actions"), 240, "function returned the correct value");
    });

    test("get ladder data test", function() {
        var node = new plf.LevelNode();
        node.branch = 1;
        var nodes = [node];
        var branch = 0;
        var element = new plf.Element(plf.CONDITIONAL, ["pink", "greenish"]);
        element.colors = element.get_default_colors();
        var ladder_data = UI.get_ladder_data(nodes, branch, element);
        deepEqual(ladder_data.length, 1 + 1*5, "data length correct");
        deepEqual(ladder_data[0].type, "ladder", "data type correct");
    });

    test("get frame content data test", function() {
        var data = UI.get_frame_content_data(["greenish"], 0);
        deepEqual(data.length, 1, "data length correct");
    });

    test("get frame content data test", function() {
        var data = UI.get_frame_content_data(["greenish"], 0);
        deepEqual(data.length, 1, "data length correct");
    });

    test("get pass node data test", function() {
        var data = UI.get_pass_node_data();
        deepEqual(data.length, 1, "data length correct");
    });

    test("get for jump inv data test", function() {
        var s_element = new plf.Element(plf.FOR_STMT, {id: 1, iterator: ["greenish"]});
        var j_element = new plf.Element(plf.FOR_JUMP, s_element);
        var data = UI.get_for_jump_inv_data(j_element);
        deepEqual(data.length, 2, "data length correct");
    });

    test("get for jump node data test", function() {
        var s_element = new plf.Element(plf.FOR_STMT, {id: 1, iterator: ["greenish"]});
        var j_element = new plf.Element(plf.FOR_JUMP, s_element);
        var data = UI.get_for_jump_node_data(j_element);
        deepEqual(data.length, 3, "data length correct");
    });

    test("get for stmt inv data test", function() {
        var s_element = new plf.Element(plf.FOR_STMT, {id: 1, iterator: ["greenish"]});
        var data = UI.get_for_stmt_inv_data(s_element);
        deepEqual(data.length, 3, "data length correct");
    });

    test("get for stmt node data test", function() {
        var s_element = new plf.Element(plf.FOR_STMT, {id: 1, iterator: ["greenish"]});
        var data = UI.get_for_stmt_node_data(s_element);
        deepEqual(data.length, 4, "data length correct");
    });

    test("get push inv data test", function() {
        var element = new plf.Element(plf.PUSH, ["greenish"]);
        element.colors = element.get_default_colors();
        var data = UI.get_push_inv_data(element);
        deepEqual(data.length, 2, "data length correct");
    });

    test("get push node data test", function() {
        var element = new plf.Element(plf.PUSH, ["greenish"]);
        element.colors = element.get_default_colors();
        var data = UI.get_push_node_data(element);
        deepEqual(data.length, 3, "data length correct");
    });

    test("get pop inv data test", function() {
        var element = new plf.Element(plf.POP);
        element.colors = element.get_default_colors();
        var data = UI.get_pop_inv_data(element);
        deepEqual(data.length, 3, "data length correct");
    });

    test("get pop node data test", function() {
        var element = new plf.Element(plf.POP);
        element.colors = element.get_default_colors();
        var data = UI.get_pop_node_data(element);
        deepEqual(data.length, 4, "data length correct");
    });

    test("get delete inv data test", function() {
        var element = new plf.Element(plf.DELETE);
        element.colors = element.get_default_colors();
        var data = UI.get_delete_inv_data(element);
        deepEqual(data.length, 1, "data length correct");
    });

    test("get delete node data test", function() {
        var element = new plf.Element(plf.DELETE);
        element.colors = element.get_default_colors();
        var data = UI.get_delete_node_data(element);
        deepEqual(data.length, 2, "data length correct");
    });

    test("get assign inv data test", function() {
        var element = new plf.Element(plf.ASSIGN, ["greenish"]);
        element.colors = element.get_default_colors();
        var data = UI.get_assign_inv_data(element);
        deepEqual(data.length, 2, "data length correct");
    });

    test("get assign node data test", function() {
        var element = new plf.Element(plf.ASSIGN, ["greenish"]);
        element.colors = element.get_default_colors();
        var data = UI.get_assign_node_data(element);
        deepEqual(data.length, 3, "data length correct");
    });

    test("get push random inv data test", function() {
        var element = new plf.Element(plf.PUSH_RANDOM, ["greenish"]);
        element.colors = element.get_default_colors();
        var data = UI.get_push_random_inv_data(element);
        deepEqual(data.length, 3, "data length correct");
    });

    test("get push random node data test", function() {
        var element = new plf.Element(plf.PUSH_RANDOM, ["greenish"]);
        element.colors = element.get_default_colors();
        var data = UI.get_push_random_node_data(element);
        deepEqual(data.length, 4, "data length correct");
    });

    test("get func call inv data test", function() {
        var element = new plf.Element(plf.FUNC_CALL);
        element.colors = element.get_default_colors();
        var data = UI.get_func_call_inv_data(element);
        deepEqual(data.length, 3, "data length correct");
    });

    test("get func call node data test", function() {
        var element = new plf.Element(plf.FUNC_CALL);
        element.colors = element.get_default_colors();
        var data = UI.get_func_call_node_data(element);
        deepEqual(data.length, 4, "data length correct");
    });

    test("get return inv data test", function() {
        var element = new plf.Element(plf.RETURN_STMT);
        element.colors = element.get_default_colors();
        var data = UI.get_return_inv_data(element);
        deepEqual(data.length, 1, "data length correct");
    });

    test("get return node data test", function() {
        var element = new plf.Element(plf.RETURN_STMT);
        element.colors = element.get_default_colors();
        var data = UI.get_return_node_data(element);
        deepEqual(data.length, 2, "data length correct");
    });

    test("get conditional inv data test", function() {
        var element = new plf.Element(plf.CONDITIONAL, ["greenish"]);
        element.colors = element.get_default_colors();
        var data = UI.get_conditional_inv_data(element);
        deepEqual(data.length, 3, "data length correct");
    });

    test("get conditional node data test", function() {
        var element = new plf.Element(plf.CONDITIONAL, ["greenish"]);
        var node = new plf.LevelNode();
        node.branch = 1;
        element.colors = element.get_default_colors();
        var data = UI.get_conditional_node_data(element, node);
        deepEqual(data.length, 2, "data length correct");
    });

    test("get sys exit inv data test", function() {
        var element = new plf.Element(plf.SYS_EXIT, ["greenish"]);
        element.colors = element.get_default_colors();
        var data = UI.get_sys_exit_inv_data(element);
        deepEqual(data.length, 3, "data length correct");
    });

    test("get sys exit node data test", function() {
        var element = new plf.Element(plf.SYS_EXIT, ["greenish"]);
        element.colors = element.get_default_colors();
        var data = UI.get_sys_exit_node_data(element);
        deepEqual(data.length, 3, "data length correct");
    });

    test("get connection node data test", function() {
        var node = new plf.LevelNode();
        var data = UI.get_connection_node_data(node);
        deepEqual(data.length, 2, "data length correct");
    });

    test("get root node data test", function() {
        var data = UI.get_root_node_data("1");
        deepEqual(data.length, 3, "data length correct");
    });

    test("get node sprite data test", function() {
        var node = new plf.LevelNode(plf.PASS);
        var data = UI.get_node_sprite_data(node, "1");
        deepEqual(data.length, 3, "data length correct");
    });

    test("get inv sprite data test", function() {
        var element = new plf.Element(plf.PASS);
        var data = UI.get_inv_sprite_data(element);
        deepEqual(data.length, 1, "data length correct");
    });
})();



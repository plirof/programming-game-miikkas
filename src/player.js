/**
   A module that defines a Player object, which describes the properties of the player's 
   character. Also contains an Inventory object that defines the player's inventory.
   
   @module player
*/

/**
   @namespace platformer
*/
this.platformer = this.platformer || {};

(function() {
    
    var p = platformer;
    
    /**
       An Inventory object, which defines an inventory of the player. The inventory contains
       four slots by colors red, blue, green and yellow, which can hold several items.
       
       @class Inventory
       @constructor
       @param red {Array} An array of items as strings to the red slot of the player.
       @param blue {Array} An array of items as strings to the blue slot of the player.
       @param green {Array} An array of items as strings to the green slot of the player.
       @param yellow {Array} An array of items as strings to the yellow slot of the player.
    */
    var Inventory = function(red, blue, green, yellow) {
        this[p.RED] = p.default_to(red, []);
        this[p.BLUE] = p.default_to(blue, []);
        this[p.GREEN] = p.default_to(green, []);
        this[p.YELLOW] = p.default_to(yellow, []);
        return this;
    };
    
    /**
       A Player object, which describes the properties of the player's character, i.e. 
       character's position and inventory.
       
       @class Player
       @constructor
       @param level {Level} The level of the game where the player is located at the start.
       @param [tree_name=MAIN] {string} The name of the node tree of the level where the 
       player is located at the start.
       @param [node_id=0] {int} The id of the node of the node tree where the player is 
       located at the start.
       @param [inventory=[]] {Object} The initial inventory object of the player.
       
       @return {Player} The initialized Player object.
    */
    var Player = function(level, tree_name, node_id, inventory) {
        this.position = {level : level,
                         tree : p.default_to(tree_name, p.MAIN),
                         id : p.default_to(node_id, 0)};
        this.inventory = p.default_to(inventory, new Inventory());
        this.callstack = [{position : this.position, inventory : this.inventory}];
        return this;
    };
    
    var ptype = Player.prototype;
    
    /**
       Makes the player character to interact with the element of their current platform
       node by executing its operations. If the operation is performed succesfully, the
       character moves to another platform determined by the element.
       
       @method interact
       @param levelrunner {LevelRunner} The object running the tests for player in the 
       level.
    */
    ptype.interact = function(levelrunner) {
        this.get_current_node().element.exec(this, levelrunner);
    };
    
    /**
       Returns the node in which the player character currently resides.
       
       @method get_current_node
       @return {LevelNode} The node that is the player's current position.
    */
    ptype.get_current_node = function() {
        return this.get_current_tree().get_node_by_id(this.position.id);
    };
    
    /**
       Returns the node tree in which the player character currently resides.
       
       @method get_current_tree
       @return {NodeTree} The node tree in which the player is currently located.
    */
    ptype.get_current_tree = function() {
        return this.position.level.get_tree_by_name(this.position.tree);
    };
    
    /**
       Updates the player character's position to a given node id and node tree with a 
       given name.
       
       @param node_id {int} The id of the new node.
       @param [tree_name] {string} The name of the new node tree.
    */
    ptype.update_position = function(node_id, tree_name) {
        this.position.id = node_id;
        this.position.tree = p.default_to(tree_name, this.position.tree);
    };
    
    /**
       Pushes the current position and inventory of the player to the top of the callstack.
       
       @method push_callstack
    */
    ptype.push_callstack = function() {
        this.callstack.push({position : {id : this.position.id, 
                                         tree : this.position.tree}, 
                             inventory : this.inventory});
    };
    
    /**
       Pops the last position and inventory of the player from the top of the callstack.
       
       @method pop_callstack
       @return {Object} The last position and inventory of the player as an anonymous object.
    */
    ptype.pop_callstack = function() {
        return this.callstack.pop();
    };
    
    /**
       Pushes a given item to a given inventory slot of the player, if possible.
       
       @method push_to_inventory
       @param color {string} The color of the slot.
       @param item {string} The name of the item.
    */
    ptype.push_to_inventory = function(color, item) {
        if (this.inventory.hasOwnProperty(color)) {
            var slot = this.inventory[color];
            if (slot.length < 4) {
                slot.push(item);
            }
        }
    };
    
    /**
       Pops the last item from a given inventory slot of the player, if possible.
       
       @method pop_from_inventory
       @param color {string} The color of the slot.
       @return {string} The name of the popped item.
    */
    ptype.pop_from_inventory = function(color) {
        if (this.inventory.hasOwnProperty(color)) {
            var slot = this.inventory[color];
            if (slot.length > 0) {
                return slot.pop();
            }
        }
        return null;
    };
    
    /**
       Sets the player's invetory to a given inventory.
       
       @method set_inventory
       @param inventory {Object} The new inventory for the player.
    */
    ptype.set_inventory = function(inventory) {
        this.inventory = inventory;
    };
    
    /**
       Sets an inventory slot of the player to contain given items.
       
       @method set_inventory_slot
       @param slot {string} The color of the slot.
       @param items {Array} An array of item names.
    */
    ptype.set_inventory_slot = function(slot, items) {
        if (this.inventory.hasOwnProperty(slot) && items.length <= 4) {
            this.inventory[slot] = items;
        }
    };
    
    /**
       Returns the contents of a requested inventory slot of the player, if possible.
       
       @method get_inventory_slot
       @param slot {string} The color of the slot.
       @return {Array} The contents of the requested slot or null if such slot does not
       exist.
    */
    ptype.get_inventory_slot = function(slot) {
        if (this.inventory.hasOwnProperty(slot)) {
            return this.inventory[slot].clone();
        }
        return null;
    };
    
    ptype.item_first_in_slot = function(item, slot) {
        if (this.inventory.hasOwnProperty(slot)) {
            var items = this.inventory[slot];
            if (items[items.length - 1] === item) {
                return true;
            }
        }
        return false;
    }
    
    /*
       Checks if a specified inventory slot of the player contains the .
       
       @method items_in_inventory
       @param slot {string} The color of the slot.
       @items items {Object} The names of the items to be checked and their required
       amounts as an anonymous object.
       @return {boolean} True if a 
    */
    /*ptype.items_in_inventory = function(slot, items) {
        if (this.inventory.hasOwnPropery(slot)) {
            var slot = this.inventory[slot].clone();
            for (item in items) {
                for (var i = 0; i < items[item]; i++) {
                    if (slot.contains(item)) {
                        slot.remove(item);
                    } else {
                        return false;
                    }
                }
            }
            return true;
        }
        return false;
    };*/
    
    platformer.Inventory = Inventory;
    platformer.Player = Player;
}());

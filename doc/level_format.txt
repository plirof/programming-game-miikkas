Requirements:

    Define json format for levels
    
    Parse levels from json
    
    Level contains:
    - game mode
    - tree for main function
    - trees for other functions
    - pre conditions
    - goal state
    - allowed operations
        * can tiles be added/changed or not
        * are some tiles uneditable
        
Main function and one additional function
{
    "name" : "",
    
    "mode" : "",
    
    "goal" : "",
    
    "opt_elements" : "",
    
    "req_elements" : "",
        
    "trees":
    {
        "main":
        [
            {"type":"pass", "params":null}, 
            {"type":"assign_inc", "params":"red"}, 
            {"type":"func_call", "params":{"args":["red"], "tree":"somefunction"}}, 
            {"type":"for_stmt", "params":["red", "red", "red"]}, 
            {"type":"pass", "params":null}, 
            {"type":"assign_inc", "params":"red"},
            {"type":"for_jump", "params":null}, 
            {"type":"pass", "params":null}, 
            {"type":"random_val", "params":["red", "yellow","violet","green"]}, 
            {"type":"pass", "params":null}, 
            {"type":"conditional", "params":[{"red" : 7}]}, 
            [
                [{"type":"pass", "params":null}], 
                [{"type":"pass", "params":null}]
            ], 
            {"type":"pass", "params":null}, 
            {"type":"sys_exit", "items":["red"]}
        ]
        "somefunction":
        [
            {"type":"pass", "params":null}, 
            {"type":"pass", "params":null}, 
            {"type":"return_stmt", "params":["red"]}
        ]
    }
}

Build mode level
{
    "name" : "some build mode level",

    "mode" : "build",
    
    // Is required end state defined as its own variable or just in the sys_exit element?
    // Does it depend on the game mode?
    "goal" : 
    {
        "blue":["red", "red", "red"],
        "red":["blue", "blue", "blue"],
        "green":["red", "green", "red"],
        "pink":["don't care"]
    },
    
    "req_elements" : 
    [
        {"type":"random_val", "params":["red", "yellow","violet","green"]}, 
        {"type":"sys_exit", "items":
                                    [
                                        "blue":["red", "red", "red"],
                                        "red":["blue", "blue", "blue"],
                                        "green":["red", "green", "red"],
                                        "pink":["don't care"]
                                    ]
        }
    ],
    
    "opt_elements":
    [
        {"type":"pass", "params":null}, 
        {"type":"assign_inc", "params":"red"}, 
        {"type":"func_call", "params":{"args":["red"], "tree":"somefunction"}}, 
        {"type":"for_stmt", "params":["red", "red", "red"]}, 
        {"type":"for_jump", "params":null}, 
        {"type":"conditional", "params":[{"red" : 7}]}, 
        {"type":"return_stmt", "params":["red"]}
    ],
    
    // Should empty trees be allowed?
    // Should each function contain at least one pass tile?
    "trees":
    {
        "main":
        [

        ]
        "somefunction":
        [

        ]
    }
    
}
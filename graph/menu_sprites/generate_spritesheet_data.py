#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Generates the EaselJS spritesheet data from input coming from stdin.

Expected input format is an endline separated list of
[imagename] [imagewidth]x[imageheight] lines (without brackets).
"""
import sys

lines = [tuple(line.strip().split(" ")) for line in sys.stdin.readlines() if line.strip()]

cumul_width = 0
frames = "platformer.MENU_FRAMES = [\n"
animations = "platformer.MENU_ANIMATIONS = {\n"
for i, line in enumerate(lines):
    width = int(line[1].split("x")[0])
    height = int(line[1].split("x")[1])
    comma = "," if i < len(lines) - 1 else ""
    frames += "    [%d,0,%d,%d]%s\n" % (cumul_width, width, height, comma)
    cumul_width += width

    animations += "    \"%s\": %d%s\n" % (line[0], i, comma)
frames += "];\n"
animations += "};\n"

if sys.argv[-1] == "--strip":
    print frames.translate(None, "\n ")
    print animations.translate(None, "\n ")
else:
    print frames
    print animations

